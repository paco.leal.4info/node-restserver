const express = require('express');

const bcrypt = require('bcrypt');
const _ = require('underscore');

const User = require('../models/user');
const { verifyToken, verifyAdmin } = require('../middleware/auth');

const app = express();

app.get('/user', verifyToken, (req, res) => {

    /*
    return res.json({
        user: req.user
    });
    */

    // paginación
    // nº de página, por defecto 0 (la 1)
    let from = req.query.from || 0;
    from = Number(from);
    // nº de objetos pedidos, por defecto 5
    let lim = req.query.lim || 5;
    lim = Number(lim);


    User.find({ status: true }, 'name email role status google img')
        .skip(from)
        .limit(lim)
        .exec((err, users) => {
            if (err) {
                return res.status(400).json({
                    ok: false,
                    err
                });
            }

            User.count({ status: true }, (err, count) => {
                res.json({
                    ok: true,
                    count,
                    users
                });
            });
        });
});

app.post('/user', [verifyToken, verifyAdmin], (req, res) => {
    let body = req.body;

    let user = new User({
        name: body.name,
        email: body.email,
        password: bcrypt.hashSync(body.password, 10),
        //img: body.img,
        role: body.role
    });

    user.save((err, userDB) => {
        if (err) {
            return res.status(400).json({
                ok: false,
                err
            });
        }

        //userDB.password = null;

        res.json({
            ok: true,
            user: userDB
        });
    });
});

app.put('/user/:id', [verifyToken, verifyAdmin], (req, res) => {
    let id = req.params.id;
    let body = _.pick(req.body, ['name', 'email', 'img', 'role', 'status']);

    User.findByIdAndUpdate(id, body, { new: true, runValidators: true }, (err, userDB) => {
        if (err) {
            return res.status(400).json({
                ok: false,
                err
            });
        }
        res.json({
            ok: true,
            user: userDB
        });
    });
});

app.delete('/user/:id', [verifyToken, verifyAdmin], (req, res) => {


    let id = req.params.id;
    let newStatus = { status: false };

    User.findByIdAndUpdate(id, newStatus, { new: true }, (err, userDB) => {
        if (err) {
            return res.status(400).json({
                ok: false,
                err
            });
        }

        res.json({
            ok: true,
            user: userDB
        });
    });

    /* DELETE FROM DATABASE
    let id = req.params.id;

    User.findByIdAndRemove(id, (err, delUser) => {
        if (err) {
            return res.status(400).json({
                ok: false,
                err
            });
        }

        if (!delUser) {
            return res.status(400).json({
                ok: false,
                err: {
                    message: 'User not found'
                }
            });
        }

        res.json({
            ok: true,
            user: delUser
        });
    });
    */
    //res.json('delete user');
});

module.exports = app;