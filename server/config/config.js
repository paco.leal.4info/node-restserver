// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~
// Puerto
// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~
process.env.PORT = process.env.PORT || 3000;

// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~
// Enviroment
// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~
process.env.NODE_ENV = process.env.NODE_ENV || "dev";

// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~
// Token_expiration (s * m * h * d)
// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~
process.env.TOKEN_EXP = 60 * 60 * 24 * 30;

// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~
// Token_seed
// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~
process.env.SEED = process.env.SEED || 'my-dev-seed';

// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~
// DB
// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~
let urlDB;

// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~
// Google Client ID
// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~
process.env.CLIENT_ID = process.env.CLIENT_ID || "253582522881-vfi9sctgritfjn0vhc37bdma2ncf0fa5.apps.googleusercontent.com";

if (process.env.NODE_ENV === 'dev') {
    urlDB = 'mongodb://localhost:27017/cafe';
} else {
    urlDB = process.env.URL_DB;
}
process.env.DB = urlDB;