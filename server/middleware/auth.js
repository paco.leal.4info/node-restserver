const jwt = require('jsonwebtoken');
// ~~~~~~~~~~~~~~~~~~~~
// Verify Token
// ~~~~~~~~~~~~~~~~~~~~

let verifyToken = (req, res, next) => {
    let token = req.get('Authorization'); // header name
    jwt.verify(token, process.env.SEED, (err, decoded) => {
        // DE AQUÍ NO PASA
        if (err) {
            return res.status(401).json({
                ok: false,
                err
                /*: {
                                    message: 'Invalid token'
                                }*/
            });
        }

        req.user = decoded.user;
        // si no ejecutamos next se para la ejecución
        next();
    });
};

// ~~~~~~~~~~~~~~~~~~~~
// Verify Admin Role
// ~~~~~~~~~~~~~~~~~~~~
let verifyAdmin = (req, res, next) => {

    let user = req.user;

    if (user.role === 'ADMIN_ROLE') {
        // si no ejecutamos next se para la ejecución
        next();
    } else {
        return res.json({
            ok: false,
            err: {
                message: 'User is not Admin'
            }
        });
    }
}

module.exports = {
    verifyToken,
    verifyAdmin
}